package com.dat255.project.android.copsandcrooks.utils;

/**
 * Constants used in the game Cops&Crooks.
 * 
 * @author Group 25, course DAT255 at Chalmers Uni.
 *
 */
public class Values {
	public static final double POLICE_CASH_REWARD_FACTOR = 0.2;
	
	public static final int GETAWAY_TICKET_COST = 5000;
	
	public static final float PAWN_MOVE_DELAY = 0.5f;
	
	 // the fixed viewport dimensions (ratio: 1.6)
    public static final int GAME_VIEWPORT_WIDTH = 800, GAME_VIEWPORT_HEIGHT = 480;

	public static final int TURNS_IN_PRISON = 4;

	public static final int DICE_RESULT_TO_ESCAPE = 6;

	public static final int MAX_TIMES_ARRESTED = 4;

	public static final float DELAY_CHANGE_PLAYER_ON_COLLISION = 3f;

	public static final float DELAY_CHANGE_PLAYER_STANDARD = 2f;

	public static final int CAR_MOVE_FACTOR = 2;

	public static final int WALKING_PAWN_MOVE_FACTOR = 1;

	public static final int MAX_DICE_ROLL = 6;

	public static final float ROLL_DELAY = 1.4f;

	public static final float DELAY_CHANGE_PLAYER_MOVE_BY_METRO = 3f;

	public static final int CASH_BANK_LOWEST = 2000;

	public static final int CASH_BANK_LOW = 5000;

	public static final int CASH_BANK_HIGH = 10000;

	public static final int CASH_BANK_HIGHEST = 20000;

	public static final int ID_COP_CAR = 20;

	public static final int ID_OFFICER = 10;

	public static final float DELAY_CHANGE_PLAYER_IN_PRISON = 3f;

    public static int TILE_WIDTH = 60, TILE_HEIGTH = 60;
}
