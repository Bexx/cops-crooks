package com.dat255.project.android.copsandcrooks.model;

/**
 * Enum representing the different roles in the game, crook and police.
 * 
 * @author Group 25, course DAT255 at Chalmers Uni.
 *
 */
public enum Role {
	Crook,
	Cop,
}
