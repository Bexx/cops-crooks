package com.dat255.project.android.copsandcrooks.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import com.dat255.project.android.copsandcrooks.utils.Point;
/**
 * A crook pawn in the game Cops&Crooks.
 * 
 * @author Group 25, course DAT255 at Chalmers Uni.
 *
 */
public class Turn implements Serializable{
	
	private static final long serialVersionUID = 55812236448851327L;
	
	public enum HideoutChoice {
		WITHDRAW,
		DEPOSIT,
		CANCEL,
	}
	
	public enum MoveType {
		METRO,
		WALK,
		NONE,
	}

	private List<Point> pathWalked;
	private Point endTilePos;
	private int pawnID, turnID;
	private HideoutChoice hideoutChoice;
	private MoveType moveType;
	private boolean escapedFromPrison;
	
	public Turn(){
		moveType = MoveType.NONE;
		hideoutChoice = HideoutChoice.CANCEL;
		endTilePos = new Point(0, 0);
	}
	
	/**
	 * Returns the type of how the pawn is moving
	 * @return the type of how the pawn is moving
	 */
	public MoveType getMoveType() {
		return moveType;
	}
	
	/**
	 * Sets the type of how the pawn is moving
	 * @param moveType the type that the pawn will be moving
	 */
	public void setMoveType(MoveType moveType) {
		this.moveType = moveType;
	}
	
	/**
	 * Returns what the players does inside a hideout
	 * @return what the player does inside a hideout
	 */
	public HideoutChoice getHideoutChoice() {
		return hideoutChoice;
	}
	
	/**
	 * Sets what the player does inside a hideout
	 * @param hideoutChoice what the player does inside a hideout
	 */
	public void setHideoutChoice(HideoutChoice hideoutChoice) {
		this.hideoutChoice = hideoutChoice;
	}
	
	/**
	 * Returns the path that the pawn has walked
	 * @return the path that the pawn has walked
	 */
	public List<Point> getPathWalked() {
		return pathWalked;
	}
	
	/**
	 * Sets the path that the pawn has walked
	 * @param tilePathWalked - the path that the pawns has walked
	 */
	public void setPathWalked(TilePath tilePathWalked) {
		this.pathWalked = new LinkedList<Point>();
		for(int i=0; i<tilePathWalked.getPathLength(); i++){
			this.pathWalked.add(tilePathWalked.getTile(i).getPosition());
		}
	}
	
	/**
	 * Returns the last tile on the path
	 * @return the last tile on the path
	 */
	public Point getEndTilePos() {
		return endTilePos;
	}
	
	/**
	 * Sets the end tile on the path
	 * @param endTile the end tile of the path
	 */
	public void setEndTile(IWalkableTile endTile) {
		endTilePos = endTile.getPosition();
	}
	
	/**
	 * Returns the ID of the pawn
	 * @return the ID of the pawn
	 */
	public int getPawnID() {
		return pawnID;
	}
	
	/**
	 * Sets the ID of the pawn
	 * @param pawnID the ID of the pawn
	 */
	public void setPawnID(int pawnID) {
		this.pawnID = pawnID;
	}
	
	/**
	 * Returns the ID of the pawn
	 * @return the ID of the pawn
	 */
	public int getTurnID() {
		return turnID;
	}
	
	/**
	 * Sets the ID of the pawn
	 * @param pawnID the ID of the pawn
	 */
	public void setTurnID(int turnID) {
		this.turnID = turnID;
	}
	
	/**
	 * Checks if the player of the turn escaped from prison.
	 * @returns true if escaped, false otherwise;
	 */
	public boolean didEscapeFromPrison() {
		return escapedFromPrison;
	}
	
	/**
	 * Checks if the player of the turn escaped from prison.
	 * @returns true if escaped, false otherwise;
	 */
	public void setEscapeFromPrison(boolean escaped) {
		this.escapedFromPrison = escaped;
	}
	
	/**
	 * Two turns are considered equal if their ID is the same!
	 */
	@Override
	public boolean equals(Object o) {
		if (o == null) {
			return false;
		} else if (o instanceof Turn) {
			Turn turn = (Turn)o;
			return turn.getTurnID() == this.getTurnID();
		}
		return false;
	}
}
