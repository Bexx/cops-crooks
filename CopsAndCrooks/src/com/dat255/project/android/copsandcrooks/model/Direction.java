package com.dat255.project.android.copsandcrooks.model;
/**
 * A crook pawn in the game Cops&Crooks.
 * 
 * @author Group 25, course DAT255 at Chalmers Uni.
 *
 */
public enum Direction {
	NORTH,
	EAST,
	SOUTH,
	WEST,
}
